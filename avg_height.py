import csv
import swapi as sw
from re import findall

table = open("height.csv","w")
writer = csv.writer(table, quoting=csv.QUOTE_NONNUMERIC)
writer.writerow(('NAME','HEIGHT','SPECIES'))



def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def get_species_string(s):
    id = int(findall('\d+',s)[0])
    return sw.get_species(id).name


heights = []
PEOPLE_COUNT = sw.get_all("people").count()
i = 1
while PEOPLE_COUNT > i :
    try:
        person_height = sw.get_person(i).height
        person_name = sw.get_person(i).name
        person_species = get_species_string(str(sw.get_person(i).species))
    except Exception:
        print("Element {} could not be loaded".format(i))
        pass
    try:
        writer.writerow((person_name,person_height, person_species))
    except Exception as e:
        print(e)
        pass
    if is_int(person_height):
        heights.append(int(person_height))
    else:
        pass
    print("Got element {} of {} elements".format(i,PEOPLE_COUNT))
    i += 1
table.close()
avg = round(sum(heights)/len(heights),3)
print(sum(heights)/len(heights))
print(avg)